﻿using System;
using System.Collections.Specialized;
using System.Linq.Expressions;

namespace Queste
{
	public class ExpressionBuilderService : IExpressionBuilderService
	{
		#region Public Methods

		/// <summary>
		/// Takes URL encoded query string and creates a dynamic Linq expression.
		/// </summary>
		/// <typeparam name="TSource">The type of object we will be building the expression against.</typeparam>
		/// <param name="queryString">A URL encoded query string</param>
		/// <param name="caseSensitive"></param>
		/// <returns>A basic linq expression for use in linq queries like Where, First, FirstOrDefault, Any, or All.</returns>
		public Expression<Func<TSource, bool>> BuildExpression<TSource>(string queryString, bool caseSensitive)
		{
			return ExpressionBuilder.BuildExpression<TSource>(queryString, caseSensitive);
		}

		public Expression<Func<TSource, bool>> BuildExpression<TSource>(NameValueCollection queryString, bool caseSensitive)
		{
			return ExpressionBuilder.BuildExpression<TSource>(queryString, caseSensitive);
		}

		/// <summary>
		/// Takes URL encoded query string and creates a dynamic Linq expression and compiles it into a function.
		/// </summary>
		/// <typeparam name="TSource">The type of object we will be building the function against.</typeparam>
		/// <param name="queryString">A URL encoded query string</param>
		/// <param name="caseSensitive"></param>
		/// <returns>A basic function for use in linq queries like Where, First, FirstOrDefault, Any, or All.</returns>
		public Func<TSource, bool> BuildFunction<TSource>(string queryString, bool caseSensitive)
		{
			return ExpressionBuilder.BuildFunction<TSource>(queryString, caseSensitive);
		}

		public Func<TSource, bool> BuildFunction<TSource>(NameValueCollection queryString, bool caseSensitive)
		{
			return ExpressionBuilder.BuildFunction<TSource>(queryString, caseSensitive);
		}
		/// <summary>
		/// Takes URL encoded query string and creates a dynamic Linq expression.
		/// </summary>
		/// <typeparam name="TSource">The type of object we will be building the expression against.</typeparam>
		/// <param name="queryString">A URL encoded query string</param>
		/// <param name="caseSensitive"></param>
		/// <returns>A basic linq expression for use in linq queries like Where, First, FirstOrDefault, Any, or All.</returns>
		public Expression<Func<TSource, bool>> BuildExpression<TSource>(string queryString)
		{
			return ExpressionBuilder.BuildExpression<TSource>(queryString);
		}

		public Expression<Func<TSource, bool>> BuildExpression<TSource>(NameValueCollection queryString)
		{
			return ExpressionBuilder.BuildExpression<TSource>(queryString);
		}

		/// <summary>
		/// Takes URL encoded query string and creates a dynamic Linq expression and compiles it into a function.
		/// </summary>
		/// <typeparam name="TSource">The type of object we will be building the function against.</typeparam>
		/// <param name="queryString">A URL encoded query string</param>
		/// <param name="caseSensitive"></param>
		/// <returns>A basic function for use in linq queries like Where, First, FirstOrDefault, Any, or All.</returns>
		public Func<TSource, bool> BuildFunction<TSource>(string queryString)
		{
			return ExpressionBuilder.BuildFunction<TSource>(queryString);
		}

		public Func<TSource, bool> BuildFunction<TSource>(NameValueCollection queryString)
		{
			return ExpressionBuilder.BuildFunction<TSource>(queryString);
		}

		#endregion
	}
}
