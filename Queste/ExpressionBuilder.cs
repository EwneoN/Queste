﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading;
using System.Web;
using static System.Linq.Expressions.Expression;

namespace Queste
{
  public static class ExpressionBuilder
  {
	  public static bool CaseSensitive = false;

	  private static readonly Dictionary<Type, Dictionary<string, PropertyInfo>> _TypeCache;
	  private static readonly ReaderWriterLockSlim _Lock;

	  static ExpressionBuilder()
	  {
			_TypeCache = new Dictionary<Type, Dictionary<string, PropertyInfo>>();
			_Lock= new ReaderWriterLockSlim(LockRecursionPolicy.NoRecursion);
	  }

		#region Public Methods

	  public static Expression<Func<TSource, bool>> BuildExpression<TSource>(NameValueCollection queryString, bool? caseSensitive = null)
	  {
			ParameterExpression parameter = Parameter(typeof(TSource), "p");

			Dictionary<string, PropertyInfo> properties;

			try
			{
				_Lock.EnterUpgradeableReadLock();

				if (!_TypeCache.TryGetValue(typeof(TSource), out properties))
				{
					_Lock.EnterWriteLock();

					//we get all the properties on TSource as we will try to match query string keys to property names
					properties = typeof(TSource).GetProperties()
																			.ToDictionary(p => p.Name.ToLower());

					_TypeCache.Add(typeof(TSource), properties);

					_Lock.ExitWriteLock();
				}
			}
			finally
			{
				_Lock.ExitUpgradeableReadLock();
			}

			Expression expression = null;

			queryString.Cast<string>()
			 .Where(key => key != null && (queryString.GetValues(key)?.Length ?? 0) > 0)
				.SelectMany(key => queryString.GetValues(key),
					(key, value) => new KeyValuePair<string, string>(key.ToLower(), value))
				.Select(kvp =>
				{
					PropertyInfo propertyInfo;

					//see if this key matches to a property on TSource
					if (!properties.TryGetValue(kvp.Key, out propertyInfo))
					{
						return null;
					}

					//we split the query string value on + as this character is used to delimit a collection of items
					string[] values = kvp.Value.Split('+');
					Type propertyType = propertyInfo.PropertyType;

					BinaryExpression or;
					BinaryExpression[] equalsArray;
					MemberExpression property = Property(parameter, propertyInfo);
					MethodCallExpression propertyToString = Call(property, nameof(ToString), null, null);

					Type[] propertyInterfaces = propertyType.GetInterfaces();
					Func<Type, bool> isIEnumerableFunc = i => i.IsGenericType &&
																										i.GetGenericTypeDefinition() == typeof(IEnumerable<>);

					// see if this propety is of type IEnumerable<> and if it is we will loop through its values 
					// for comparison with query string value items
					if (propertyInterfaces.Any(isIEnumerableFunc) &&
							typeof(string) != propertyType)
					{
						Type iEnumerableType = propertyInterfaces.FirstOrDefault(isIEnumerableFunc);
						Type itemType = iEnumerableType?.GetGenericArguments()[0];

						//if for whatever reason we have failed to get an element type just return null
						if (itemType == null)
						{
							return (Expression)null;
						}

						//create a parameter expression for value items
						//also create a ToString call expression for this param expression
						ParameterExpression item = Parameter(itemType, "e");
						MethodCallExpression itemToString = Call(item, nameof(ToString), null, null);

						var propertyNotNullExpression = NotEqual(property, Constant(null));

						//get our func type ready using the value item type
						var func = typeof(Func<,>).MakeGenericType(itemType, typeof(bool));

						if (values.Length == 1)
						{
							BinaryExpression equals = BuildEqualExpression(itemType, values[0], item, itemToString, caseSensitive ?? CaseSensitive);

							// .Any(e => e == values[0]) 
							// .Any(e => e.ToString() == values[0])
							return AndAlso(propertyNotNullExpression, Call(typeof(Enumerable), nameof(Enumerable.Any),
								new[] { itemType }, property, Lambda(func, equals, item)));
						}

						// e == values[0]
						// e.ToString() == values[0]
						equalsArray = values
							.Select(value => BuildEqualExpression(itemType, value, item, itemToString, caseSensitive ?? CaseSensitive))
							.ToArray();

						// (e == values[0] || e == value[1])
						// (e.ToString() == values[0] || e.ToString() == values[1])
						or = Or(equalsArray.First(), equalsArray.Last());

						for (int i = equalsArray.Length - 2; i > 0; i--)
						{
							// (e == values[0] || e == values[1] || e == values[2])
							// (e.ToString() == values[0] || e.ToString() == values[1] || e.ToString() == values[2])
							or = Or(equalsArray[i], or);
						}

						// .Any(e == values[0] || e == values[1] || e == values[2])
						// .Any(e.ToString() == values[0] || e.ToString() == values[1] || e.ToString() == values[2])
						return AndAlso(propertyNotNullExpression, Call(typeof(Enumerable), nameof(Enumerable.Any),
							new[] { itemType }, property, Lambda(func, or, item)));
					}

					if (values.Length == 1)
					{
						// e == values[0]
						// e.ToString() == values[0]
						return BuildEqualExpression(propertyType, kvp.Value, property, propertyToString, caseSensitive ?? CaseSensitive);
					}

					// e == values[0]
					// e.ToString() == values[0]
					equalsArray = values
						.Select(value => BuildEqualExpression(propertyType, value, property, propertyToString, caseSensitive ?? CaseSensitive))
						.ToArray();

					// (e == values[0] || e == value[1])
					// (e.ToString() == values[0] || e.ToString() == values[1])
					or = Or(equalsArray.First(), equalsArray.Last());

					for (int i = equalsArray.Length - 2; i > 0; i--)
					{
						// (e == values[0] || e == values[1] || e == values[2])
						// (e.ToString() == values[0] || e.ToString() == values[1] || e.ToString() == values[2])
						or = Or(equalsArray[i], or);
					}

					// (e == values[0] || e == values[1] || e == values[2])
					// (e.ToString() == values[0] || e.ToString() == values[1] || e.ToString() == values[2])
					return or;
				})
				.ForEach(e =>
				{
					if (e == null)
					{
						return;
					}

					if (expression == null)
					{
						expression = e;
						return;
					}

					expression = And(e, expression);
				});

			return expression != null
				? Lambda<Func<TSource, bool>>(expression, parameter)
				: p => false;
		}

		/// <summary>
		/// Takes URL encoded query string and creates a dynamic Linq expression.
		/// </summary>
		/// <typeparam name="TSource">The type of object we will be building the expression against.</typeparam>
		/// <param name="queryString">A URL encoded query string</param>
		/// <returns>A basic linq expression for use in linq queries like Where, First, FirstOrDefault, Any, or All.</returns>
		public static Expression<Func<TSource, bool>> BuildExpression<TSource>(string queryString, bool? caseSensitive = null)
    {
      if (string.IsNullOrWhiteSpace(queryString))
      {
        return p => false;
      }

      queryString = queryString.Replace("%3d", "=");
      queryString = queryString.Replace("+", "%2B");

      NameValueCollection queryStringPairs = HttpUtility.ParseQueryString(queryString);

	    return BuildExpression<TSource>(queryStringPairs, caseSensitive);
    }

	  public static Func<TSource, bool> BuildFunction<TSource>(NameValueCollection queryString, bool? caseSensitive = null)
		{
			return BuildExpression<TSource>(queryString, caseSensitive)?.Compile();
		}

		/// <summary>
		/// Takes URL encoded query string and creates a dynamic Linq expression and compiles it into a function.
		/// </summary>
		/// <typeparam name="TSource">The type of object we will be building the function against.</typeparam>
		/// <param name="queryString">A URL encoded query string</param>
		/// <returns>A basic function for use in linq queries like Where, First, FirstOrDefault, Any, or All.</returns>
		public static Func<TSource, bool> BuildFunction<TSource>(string queryString, bool? caseSensitive = null)
    {
      return BuildExpression<TSource>(queryString, caseSensitive)?.Compile();
    }

    #endregion

    #region Private Methods

    private static BinaryExpression BuildEqualExpression(Type type, string queryValue,
      Expression parameterExpression, MethodCallExpression toStringExpression, bool caseSensitive)
    {
      ConstantExpression valueExpression;

      bool typeCanBeConst = type.GetInterface(nameof(IConvertible)) != null &&
                            type.IsValueType || type == typeof(string) || type == typeof(Guid);

      if (typeCanBeConst)
      {
        if (typeof(string) == type)
        {
          return BuildStringEqualExpression(parameterExpression, Constant(queryValue), caseSensitive);
        }

        if (typeof(Guid) == type)
        {
          return Equal(parameterExpression, Constant(new Guid(queryValue)));
        }

        return Equal(parameterExpression, Constant(System.Convert.ChangeType(queryValue, type)));
      }

      valueExpression = Constant(queryValue);

      if (type.IsValueType)
      {
        return BuildStringEqualExpression(toStringExpression, valueExpression, caseSensitive);
      }

      Expression nullExpression = Constant(null);

      return AndAlso(NotEqual(parameterExpression, nullExpression),
        BuildStringEqualExpression(toStringExpression, valueExpression, caseSensitive));
    }


    private static BinaryExpression BuildStringEqualExpression(Expression parameterExpression,
                                                               ConstantExpression valueExpression, bool caseSensitive)
    {
      if (caseSensitive)
      {
        return Equal(parameterExpression, valueExpression);
      }

      var stringEquals = typeof(string).GetMethod(nameof(Equals), new[] { typeof(string), typeof(string), typeof(StringComparison) });
      var ordinalEnum = Constant(StringComparison.OrdinalIgnoreCase);
      var trueVal = Constant(true);
      MethodCallExpression stringEqualsCall = Call(stringEquals, parameterExpression, valueExpression, ordinalEnum);

      return Equal(stringEqualsCall, trueVal);
    }

    #endregion
  }
}