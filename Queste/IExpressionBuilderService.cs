﻿using System;
using System.Collections.Specialized;
using System.Linq.Expressions;

namespace Queste {
	public interface IExpressionBuilderService {
		/// <summary>
		/// Takes URL encoded query string and creates a dynamic Linq expression.
		/// </summary>
		/// <typeparam name="TSource">The type of object we will be building the expression against.</typeparam>
		/// <param name="queryString">A URL encoded query string</param>
		/// <param name="caseSensitive"></param>
		/// <returns>A basic linq expression for use in linq queries like Where, First, FirstOrDefault, Any, or All.</returns>
		Expression<Func<TSource, bool>> BuildExpression<TSource>(NameValueCollection queryString);
		Expression<Func<TSource, bool>> BuildExpression<TSource>(string queryString);
		Expression<Func<TSource, bool>> BuildExpression<TSource>(NameValueCollection queryString, bool caseSensitive);
		Expression<Func<TSource, bool>> BuildExpression<TSource>(string queryString, bool caseSensitive);

		/// <summary>
		/// Takes URL encoded query string and creates a dynamic Linq expression and compiles it into a function.
		/// </summary>
		/// <typeparam name="TSource">The type of object we will be building the function against.</typeparam>
		/// <param name="queryString">A URL encoded query string</param>
		/// <param name="caseSensitive"></param>
		/// <returns>A basic function for use in linq queries like Where, First, FirstOrDefault, Any, or All.</returns>
		Func<TSource, bool> BuildFunction<TSource>(NameValueCollection queryString);
		Func<TSource, bool> BuildFunction<TSource>(string queryString);
		Func<TSource, bool> BuildFunction<TSource>(NameValueCollection queryString, bool caseSensitive);
		Func<TSource, bool> BuildFunction<TSource>(string queryString, bool caseSensitive);
	}
}